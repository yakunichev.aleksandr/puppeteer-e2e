const timeout = 10000;

beforeAll(async () => {
  await page.goto('http://127.0.0.1:3000', {waitUntil: 'domcontentloaded'});
});

describe('Test title and button of the homepage', () => {
  test('Title of the page', async () => {
    const title = await page.title();
    expect(title).toBe('JEST-PUPPETEER Test Yopta');
  }, timeout);

  test('Button exists and clicks', async () => {
    const $button = await page.$('button');
    const html = await page.evaluate($button => $button.innerHTML, $button);
    expect(html).toBe('Count 0');
    const html2 = await $button.evaluate($button => {
      $button.click();
      return $button.innerHTML;
    });
    expect(html2).toBe('Count 1');
  }, timeout);
});
