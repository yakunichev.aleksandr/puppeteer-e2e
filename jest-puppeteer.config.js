module.exports = {
  server: {
    command: 'http-server -g -p 3000 .',
  },
  launch: {
    headless: true,
    slowMo: false,
    devtools: false,
  },
};
